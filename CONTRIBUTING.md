Ansible
=======

Dependencies
------------

Install and configure Docker or Buildah

Test'n dev
----------

Using Docker :

```sh
docker build --pull --iidfile /tmp/iid .
docker run --rm -t \
    --privileged \
    -v /run/docker.sock:/run/docker.sock \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v $PWD/verify.yml:/verify.yml \
    $(cat /tmp/iid) \
    ansible-playbook verify.yml
```

Using Podman :

```sh
systemctl start --user podman.socket
podman build --pull --iidfile /tmp/iid .
podman run --rm -t \
    --privileged \
    -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v $PWD/verify.yml:/verify.yml \
    $(cat /tmp/iid) \
    ansible-playbook verify.yml
```
