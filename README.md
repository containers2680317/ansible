Ansible
=======

[![pipeline status](https://gitlab.com/yoanncolin/container-images/ansible/badges/main/pipeline.svg)](https://gitlab.com/yoanncolin/container-images/ansible/-/commits/main)

Container image with [Ansible][] and CLI for Docker, Podman and OpenStack.

GitLab Project : [yoanncolin/container-images/ansible][].

[Ansible]: https://github.com/ansible
[yoanncolin/container-images/ansible]: https://gitlab.com/yoanncolin/container-images/ansible

Tags
----

* [`2.14.4`][latest], [`2.14`][latest], [`2`][latest], [`latest`][latest] (Weekly build)
* [`2.13.6`][2.13], [`2.13`][2.13] (Monthly build)
* [`2.13.0`][2.13.0]
* [`2.11.6`][2.11], [`2.11`][2.11] (Monthly build)
* [`2.10.5`][2.10], [`2.10`][2.10]

[latest]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/main/Dockerfile
[2.13]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.13/Dockerfile
[2.13.0]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.13.0/Dockerfile
[2.11]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.11/Dockerfile
[2.10]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.10/Dockerfile

Software versions
-----------------

|   Ansible    | Alpine |   Docker   | Podman  | Openstack-CLI |  Python   |
| :----------: | :----: | :--------: | :-----: | :-----------: | :-------: |
| **`2.14.4`** | `edge` |  `23.0.2`  | `4.4.4` |   Antelope    | `3.11.2`  |
| **`2.13.6`** | `3.17` | `20.10.21` | `4.3.1` |      Zed      | `3.10.10` |
| **`2.13.0`** | `3.16` | `20.10.21` | `4.1.0` |     Yoga      | `3.10.5`  |
| **`2.11.6`** | `3.15` | `20.10.16` | `3.4.7` |     Xena      | `3.9.16`  |
| **`2.10.5`** | `3.14` | `20.10.11` | `3.2.3` |    Wallaby    | `3.9.16`  |

Usage
-----

Simple usages

```sh
docker run gwerlas/ansible ansible --version
docker run gwerlas/ansible ansible-galaxy collection list
docker run -v <my-dir>:/work -w /work ansible-playbook <my-play>.yml
```

Docker / Podman in Docker :

```sh
docker run \
  --privileged \
  -v /run/docker.sock:/run/docker.sock \
  -v /sys/fs/cgroup:/sys/fs/cgroup \
  gwerlas/ansible ...
```

Docker / Podman in rootless Podman :

```
systemctl start --user podman.service
podman run \
  --privileged \
  -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
  -v /sys/fs/cgroup:/sys/fs/cgroup \
  gwerlas/ansible ...
```

### Gitlab-CI

```yaml
my-job:
  image: gwerlas/ansible
  script:
    - ansible-galaxy install -r requirements.txt
    - ansible-playbook playbooks/site.yml
```

With Docker :

```yaml
my-job:
  image: gwerlas/ansible
  services:
    - docker:20.10-dind
  script:
    - ansible-galaxy install -r requirements.txt
    - ansible-playbook playbooks/site.yml
```

Builts in
---------

### System tools

- buildah
- docker
- git
- goss
- openstack
- openssl
- podman
- rsync
- ssh
- sshpass
- yq

### Ansible modules

- yay

### Collections

- amazon.aws
- ansible.netcommon
- ansible.posix
- ansible.utils (Since `2.11`)
- ansible.windows
- arista.eos
- awx.awx
- azure.azcollection
- check_point.mgmt
- chocolatey.chocolatey
- cisco.aci
- cisco.asa
- cisco.dnac (Since `2.13.6`)
- cisco.intersight
- cisco.ios
- cisco.iosxr
- cisco.ise (Since `2.13`)
- cisco.meraki
- cisco.mso
- cisco.nso
- cisco.nxos
- cisco.ucs
- cloud.common (Since `2.13`)
- cloudscale_ch.cloud
- community.aws
- community.azure
- community.ciscosmb (Since `2.13`)
- community.crypto
- community.digitalocean
- community.dns (Since `2.13`)
- community.docker
- community.fortios
- community.general
- community.google
- community.grafana
- community.hashi_vault
- community.hrobot
- community.libvirt
- community.mongodb
- community.mysql
- community.network
- community.okd
- community.postgresql
- community.proxysql
- community.rabbitmq
- community.routeros
- community.sap (Since `2.13`)
- community.sap_libs (Since `2.13.6`)
- community.skydive
- community.sops (Since `2.11`)
- community.vmware
- community.windows
- community.zabbix
- containers.podman
- cyberark.conjur
- cyberark.pas
- dellemc.enterprise_sonic (Since `2.11`)
- dellemc.openmanage (Since `2.11`)
- dellemc.os10
- dellemc.os6
- dellemc.os9
- f5networks.f5_modules
- fortinet.fortimanager
- fortinet.fortios
- frr.frr
- gluster.gluster
- google.cloud
- hetzner.hcloud
- hpe.nimble (Since `2.11`)
- ibm.qradar
- infinidat.infinibox
- infoblox.nios_modules (Since `2.13`)
- inspur.ispim (Since `2.13.6`)
- inspur.sm (Since `2.11`)
- junipernetworks.junos
- kubernetes.core (Since `2.11`)
- lowlydba.sqlserver (Since `2.13.6`)
- mellanox.onyx
- netapp.aws
- netapp.azure (Since `2.11`)
- netapp.cloudmanager (Since `2.11`)
- netapp.elementsw
- netapp.ontap
- netapp.storagegrid (Since `2.13`)
- netapp.um_info (Since `2.11`)
- netapp_eseries.santricity
- netbox.netbox
- ngine_io.cloudstack
- ngine_io.exoscale
- ngine_io.vultr
- openstack.cloud
- openvswitch.openvswitch
- ovirt.ovirt
- purestorage.flasharray
- purestorage.flashblade
- purestorage.fusion (Since `2.13.6`)
- sensu.sensu_go (Since `2.11`)
- servicenow.servicenow
- splunk.es
- t_systems_mms.icinga_director (Since `2.11`)
- theforeman.foreman
- vmware.vmware_rest (Since `2.13`)
- vyos.vyos
- wti.remote

### OpenStack clients

* [barbican](https://wiki.openstack.org/wiki/barbican)
* [cinder](https://wiki.openstack.org/wiki/cinder)
* [designate](https://wiki.openstack.org/wiki/designate)
* [freezer](https://wiki.openstack.org/wiki/freezer)
* [glance](https://wiki.openstack.org/wiki/glance)
* [heat](https://wiki.openstack.org/wiki/heat)
* [ironic](https://wiki.openstack.org/wiki/ironic)
* [keystone](https://wiki.openstack.org/wiki/keystone)
* [magnum](https://wiki.openstack.org/wiki/magnum)
* [mistral](https://docs.openstack.org/mistral/yoga/)
* [monasca](https://wiki.openstack.org/wiki/monasca)
* [neutron](https://docs.openstack.org/neutron/yoga/)
* [nova](https://wiki.openstack.org/wiki/nova)
* [octavia](https://wiki.openstack.org/wiki/octavia)
* [sahara](https://docs.openstack.org/sahara/yoga/)
* [solum](https://wiki.openstack.org/wiki/solum)
* [swift](https://wiki.openstack.org/wiki/swift)
* [tacker](https://wiki.openstack.org/wiki/tacker)
* [trove](https://wiki.openstack.org/wiki/trove)
* [vitrage](https://wiki.openstack.org/wiki/vitrage)
* [zaqar](https://wiki.openstack.org/wiki/zaqar)
* [zun](https://wiki.openstack.org/wiki/zun)

License
-------

[BSD 3-Clause License](LICENSE).
